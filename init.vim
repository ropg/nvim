" ==================================================
" .nvimrc
" ==================================================

" ==== Pathogen:
execute pathogen#infect()

" ==== Theme:
syntax on
set background=dark
set t_Co=256
colorscheme PaperColor

" ==== AirLine:
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='PaperColor'

" ==== Syntastic:
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" == Extras:
let g:syntastic_make_checkers = ['gnumake']
let g:syntastic_javascript_checkers = ['json_tool', 'jshint']
let g:syntastic_yaml_checkers = ['pyyaml']
let g:syntastic_c_checkers = ['clang_check']
let g:syntastic_cpp_checkers = ['clang_check']
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'

" ==== vim.cpp:
let g:cpp_class_scope_highlight = 0
let g:cpp_experimental_template_highlight = 0

" ==== UltiSnips
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-s>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" ==== vim-easymotion
let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Bi-directional find motion
" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
nmap s <Plug>(easymotion-s)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
nmap s <Plug>(easymotion-s2)

" Turn on case insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

" ==== YouCompleteMe
let g:ycm_collect_identifiers_from_tags_files = 1

" ==== Indent Guides
let g:indentLine_enabled = 1
let g:indentLine_leadingSpaceEnabled = 1

" ==== javascript-libraries
let g:used_javascript_libs = 'underscore,angularjs,angularui,jquery'

" ==== toggle invisible characters
set invlist
set listchars=tab:▸\ ,eol:¬,trail:⋅,extends:❯,precedes:❮
highlight SpecialKey ctermbg=none " make the highlighting of tabs less annoying
set showbreak=↪
nmap <leader>l :set list!<cr><Paste>

" ==== Etc.
filetype plugin indent on

set encoding=utf8
set linespace=4
set shiftwidth=4
set expandtab
set colorcolumn=80
set number
set autoread
set ignorecase
set smartcase
set incsearch
set cursorline
set mouse=c
set autoread
set backspace=indent,eol,start

" faster redrawing
set ttyfast
set diffopt+=vertical

" disable Ex mode
noremap Q <NOP>

let mapleader=","

let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1

" rainbow parentheses
let g:rbpt_max = 16

au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" Neovim terminal
tnoremap <Esc> <C-\><C-n>
